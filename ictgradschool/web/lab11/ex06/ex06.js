"use strict";


$(document).ready(function () {
    // $("form").submit(function (event) {
    //     var emailID = $("#emailID").val().toLowerCase();

    //     if ((emailID.indexOf("@auckland.ac.nz") > -1) ||
    //         (emailID.indexOf("@aucklanduni.ac.nz") > -1) ||
    //         (emailID.indexOf("@waikato.ac.nz") > -1)) {
    //         $("#emailID").css("border-color", "green");
    //         console.log("String Found");
    //         return true;
    //     }
    //     else {
    //         console.log("String Not Found");
    //         $("#emailID").css("border-color", "red");
    //         alert("Enter a valid university email address");
    //         event.preventDefault(); // Cance the submission
    //         return false;
    //     }


    // });
    var inputs = $("#emailID");
    $("#emailID").blur(function () {
        var emailID = $(this).val().toLowerCase();
        if ((emailID.indexOf("@auckland.ac.nz") > -1) ||
            (emailID.indexOf("@aucklanduni.ac.nz") > -1) ||
            (emailID.indexOf("@waikato.ac.nz") > -1)) {
            if ($(this).hasClass("interacted_invalid")) {
                //do something it does have the interacted_invalid class!
                $(this).removeClass("interacted_invalid");
                console.log("i have the interacted_invalid class");
            }
            $(this).addClass("interacted_valid");
            

        }
        else {
            if ($(this).hasClass("interacted_valid")) {
                //do something it does have the interacted_valid class!
                $(this).removeClass("interacted_valid");
                console.log("i have the interacted_valid class");
            }
            $(this).get(0).setCustomValidity("Please enter a valid @auckland.ac.nz,  @aucklanduni.ac.nz, or @waikato.ac.nz email address");
            alert($(this).validationMessage);
            $(this).addClass("interacted_invalid");

        }
    });

});