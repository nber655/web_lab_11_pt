"use strict";


$(document).ready(function () {
    $("form").submit(function (event) {
        var emailID = $("#emailID").val().toLowerCase();

        if ((emailID.indexOf("@auckland.ac.nz") > -1) ||
            (emailID.indexOf("@aucklanduni.ac.nz") > -1) ||
            (emailID.indexOf("@waikato.ac.nz") > -1)) {
            $("#emailID").css("border-color", "green");
            console.log("String Found");
            return true;
        }
        else {
            console.log("String Not Found");
            $("#emailID").css("border-color", "red");
            alert("Enter a valid university email address");
            event.preventDefault(); // Cance the submission
            return false;
        }


    });
});